grammar FOOL;  

@header{
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import ast.*;
import lib.FOOLlib;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;
}

@parser::members{
int stErrors=0; //errori di parsing

// SYMBOL TABLE
private int nestingLevel = 0;
private List<Map<String,STentry>> symTable = new ArrayList<Map<String,STentry>>();
//livello ambiente con dichiarazioni piu' esterno e' 0 (prima posizione ArrayList) invece che 1 (slides)
//il fronte della lista di tabelle e' symTable.get(nestingLevel)

// CLASS TABLE
private Map<String, Map<String,STentry>> classTable = new HashMap<>( );

private int offset = -2; //scelta di progettazione
}

@lexer::members {
int lexicalErrors=0; //errori lessicali
}

/*------------------------------------------------------------------
 * PARSER RULES
 *------------------------------------------------------------------*/

/* 
 * NODO PROGRAMMA 
 * Il nodo programma ritorna un nodo che puo' essere ProgNode o ProgLetInNode a seconda
 * che sia solo un'espressione, o che ci siano anche dichiarazioni.
 *  
 */

prog returns [Node ast] : {
	
    	symTable.add(new HashMap<String,STentry>()); //aggiunge un nestingLevel iniziale aggiungendo
    												//la prima tabella
	}
	(
		e=exp { $ast = new ProgNode( $e.ast ); } 
   	|	LET { List<Node> declist = new ArrayList<Node>( ); }
   			( 	
   				cls = cllist { declist.addAll( $cls.astList ); } //si aggiunge alla lista delle dichiarazioni
   																	//la lista delle dichiarazioni delle classi
   				( dec = declist { declist.addAll( $dec.astList ); } )?//lista di dichiarazioni di funzioni e 
   																		//variabili opzionale se segue la dichiarazioni
   																		//di classi
    		| 	dec = declist { declist.addAll( $dec.astList ); } //lista di dichiarazioni di funzioni e 
   																		//variabili	 			
    		)
    	IN e = exp { $ast = new ProgLetInNode(declist, $e.ast ); } //corpo del programma dove vengono 
    																//richiamate le cose dichiarate sopra    
	) { symTable.remove( nestingLevel ); } //rimuove il nesting level iniziale
    SEMIC EOF ;

/*
 * NODO LISTA CLASSI
 * Nodo per la dichiarazioni delle classi.
 */

cllist returns [List<Node> astList] : {
		$astList = new ArrayList<Node>();

		Map<String,STentry> symbolTableLevelZero = symTable.get(0); //prende l'entry corrispondente al nestingLevel 
																	//che e' sempre 0
	   
	} (
	CLASS clsID = ID {
		
		ClassTypeNode classTypeNode = new ClassTypeNode();
		ClassNode classNode = new ClassNode(classTypeNode, $clsID.text);
		
		int fieldOffset = -1; //campi partono da offset -1 e hanno offset negativi
	   	int methodOffset = 0; //metodi partono da offset 0 e hanno offset positivi
	   	
	   	Map<String,STentry> virtualTable = new HashMap<>();	//istanzia la virtual table
		symTable.add(virtualTable); //e la aggiungo alla symbol table
	   	
	   	Set<String> errorRedefinition = new HashSet<>(); //variabile per verificare la redifinizione erronea di campi e metodi	
		
		//Mette in symbol table la entry del tipo della classe
		if (symbolTableLevelZero.put( $clsID.text, new STentry(nestingLevel, classTypeNode, offset--, false ) ) != null ) {
	        System.out.println("Class "+$clsID.text+" already declared at line "+$clsID.line);
	    	stErrors++;
	    }
	    	
      	nestingLevel++; //Incremento il nestingLevel  	
		
	} ( EXTENDS supID = ID { //se estende un'altra classe
		//con classTable.get(id) prende la virtualTable di tale classe e controlla se non e' null
		if (classTable.get( $supID.text ) == null ) { //se la classe che estende non esiste da' errore											
			System.out.println( "Class " + $supID.text + " not found at line " + $supID.line );
    		stErrors++;
		} 
		
		classNode.setSuper(symbolTableLevelZero.get($supID.text)); //setta il supertipo che prende dalla symbolTable al livello 0
		ClassTypeNode superClassType = (ClassTypeNode) symbolTableLevelZero.get( $supID.text ).getType(); //setta il tipo della superclasse
        
         //aggiunge gli elementi(campi e metodi) del sopratipo alla virtual table della sottoclasse
        classTable.get($supID.text).forEach((entryID, entryType) -> virtualTable.put(entryID, entryType)); 
        FOOLlib.superType.put($clsID.text, $supID.text); //setta la superclasse come padre della classe attuale
        
        /*
         *  Nel caso erediti campi o metodi l'offset da cui parte deve essere successivo 
         * a quello di tali campi e metodi.
         */
        
        fieldOffset = - superClassType.getAllFields().size() - 1; 
        methodOffset = superClassType.getAllMethods().size();
        
       
		
	} )?
	LPAR (
		/*
		 * CAMPI
		 * Specifica i parametri passati al costruttore e il relativo tipo
		 */
		fldID1 = ID COLON fldT1 = type { //primo campo della classe e relativo tipo
			FieldNode fieldNode = new FieldNode($fldID1.text, $fldT1.ast);
			classNode.setField(fieldNode); //aggiunge il campo alla classe
			
			errorRedefinition.add($fldID1.text); //lo aggiunge per evitare che i campi dopo si chiamino nello stesso modo
				
			STentry superElemName = virtualTable.get($fldID1.text); //prende l'eventuale elemento con tale nome
																	//puo' essere sia campo che metodo(ereditati dal padre, essendo
								  									//il primo campo)			
			//essendo il primo campo, se trova qualcosa in virtual table e' per forza ereditata
			//quindi non c'e' bisogno di controllare che il padre non sia null										
			if(superElemName != null){ 
				//controlla che non sia un metodo
				if(superElemName.isMethod()){
					System.out.println( "Field " + $fldID1.text + " with same name of a method at line " + $fldID1.line );
    				stErrors++;
				}	
				fieldNode.setOffset(superElemName.getOffset()); //quindi l'offset e' lo stesso del campo nella classe padre	
				//sovrascrive quello messo precedentemente
				virtualTable.put($fldID1.text, new STentry(nestingLevel, $fldT1.ast, superElemName.getOffset(), false)); 
																						
			}	 
			else { //superElemName==null non c'e' override
				fieldNode.setOffset(fieldOffset);
				virtualTable.put($fldID1.text, new STentry(nestingLevel, $fldT1.ast, fieldOffset--, false));
			}		
			
		} ( COMMA fldIDn = ID COLON fldTn = type { //dal secondo campo in poi
			fieldNode = new FieldNode($fldIDn.text, $fldTn.ast);
			classNode.setField(fieldNode); //aggiunge il campo alla classe
			
			if(errorRedefinition.stream().anyMatch(s -> s.equals($fldIDn.text))){
				System.out.println( "Field " + $fldIDn.text + " redefined at line " + $fldIDn.line );
    			stErrors++;
			}
			else {
				errorRedefinition.add($fldIDn.text);
			}
			
			//Fa come sopra, controlla se estende e se fa override dei campi
			superElemName = virtualTable.get($fldIDn.text); //prende l'eventuale elemento con tale nome
															//puo' essere sia campo che metodo(ereditati dal padre)
															//perche' sul campo c'e' gia' il controllo con errorRedefinition
			//siamo sicuri che se la classe eredita, e trova qualcosa in virtual table 
			//e' per forza ereditata perche' effettuiamo gia' il controllo che non sia un campo di questa classe
			//con errorRedefinition
			if(superElemName != null){
				//controlla che non sia un metodo
				if(superElemName.isMethod()){
					System.out.println( "Field " + $fldIDn.text + " with same name of a method at line " + $fldIDn.line );
    				stErrors++;
				}
				fieldNode.setOffset(superElemName.getOffset());//quindi l'offset e' lo stesso del campo nella classe padre	
				//sovrascrive quello messo precedentemente
				virtualTable.put($fldIDn.text, new STentry(nestingLevel, $fldTn.ast, superElemName.getOffset(), false));	
			} else {
				fieldNode.setOffset(fieldOffset);
				virtualTable.put($fldIDn.text, new STentry(nestingLevel, $fldTn.ast, fieldOffset--, false));
			}
		})*
	)? RPAR
	CLPAR 
	/*
	 * METODI
	 * Dichiarazione dei metodi della classe.
	 * 
	 */
		(
			FUN metID = ID COLON metT = type {
				List<Node> methodParTypes = new ArrayList<>(); //crea la lista dei parametri del metodo
				
				MethodNode methodNode = new MethodNode($metID.text, $metT.ast); //crea un MethodNode
				classNode.setMethod(methodNode); //lo aggiunge tra i metodi della classe
				
				//controlla che il nome di tale metodo non sia gia' il nome di un campo o di un altro metodo
				//della stessa classe
				if(errorRedefinition.stream().anyMatch(s -> s.equals($metID.text))){
					System.out.println( "Method " + $metID.text + " redefined at line " + $metID.line );
    				stErrors++;
				}
				else {
					errorRedefinition.add($metID.text);
				}
		
				//stessa cosa dei campi: prendiamo dalla virtual table un eventuale elemento con lo stesso nome
				//siamo sicuri che se la classe eredita e trova qualcosa in virtual table e' per forza nella virtual
				//table del padre, perche' abbiamo gia' fatto il controllo di questa classe con errorRedefinition
				STentry superElemName = virtualTable.get($metID.text); 
				if(superElemName != null){
					//se non e' un metodo
					if(!superElemName.isMethod()){
						System.out.println( "Method " + $metID.text + " with same name of a field at line " + $metID.line );
    					stErrors++;
					}
					methodNode.setOffset(superElemName.getOffset()); //se eredita setta l'offset a quello del padre
					//sostituisce in virtual table il metodo overridato
					virtualTable.put($metID.text, new STentry(nestingLevel, new ArrowTypeNode( methodNode.getPars( ), $metT.ast ), superElemName.getOffset(), true));
				} else {
					methodNode.setOffset(methodOffset); //setta l'offset a quello attuale
					//aggiunge alla virtual table
					virtualTable.put($metID.text, new STentry(nestingLevel, new ArrowTypeNode( methodNode.getPars( ), $metT.ast ), methodOffset++, true));
				}
				
				Map<String,STentry> symbolTableMethodLevel = new HashMap<String,STentry> (); //hashmap per lo scope del metodo
		       	symTable.add(symbolTableMethodLevel);
				
		       	//incrementa il nestingLevel e aggiunge la tabella alla symbol table che corrisponde allo scope
		       	//del corpo dei metodi
		       	nestingLevel++;
		        
		 	}
			LPAR { int parOffset = 1; } //offset dei parametri perche' non c'e' la ridefinizione
				( parID1 = ID COLON parT1 = hotype { //primo parametro e il relativo tipo hottype perche'
													//e' piu' generico di type
		        	methodParTypes.add( $parT1.ast ); //aggiunge il parametro alla lista dei parametri del metodo
		         	ParNode parNode = new ParNode( $parID1.text, $parT1.ast ); 	//crea il nodo parametro
		          	methodNode.addPar( parNode ); //aggiunge il parametro al nodo metodo
		           	if ( symbolTableMethodLevel.put( $parID1.text, new STentry( nestingLevel, $parT1.ast, parOffset++, false ) ) != null  ) { //aggiunge la dichiarazione a hmn
		           		System.out.println( "Parameter " + $parID1.text + " already declared at line " + $parID1.line);
		        		stErrors++;
		        	}
		       	}
		      	( COMMA parIDn = ID COLON parTn = hotype { //dal secondo parametro
		      		methodParTypes.add( $parTn.ast ); //lo aggiunge
		         	ParNode par = new ParNode( $parIDn.text, $parTn.ast ); //crea il nodo parametro
		          	methodNode.addPar( par ); //lo aggiunge al nodo metodo
		           	if ( symbolTableMethodLevel.put( $parIDn.text, new STentry( nestingLevel, $parTn.ast, parOffset++, false  ) ) != null ) { //aggiunge la dichiarazione a hmn
		           		System.out.println( "Parameter " + $parIDn.text + " already declared at line " + $parIDn.line);
		        		stErrors++;
		        	}
		      	} )* )?
			RPAR
		    ( 
		    	//entrando nello scope di dichiarazioni di variabili nei metodi
		    	//entrando in un nuovo scope l'offset riparte da -2
		    	LET {
			    	int oldOffset = offset;
			    	offset = -2;
		    	} (
		    		//dichiara le variabili come id: tipo = exp
		    		VAR varID = ID COLON varT = type ASS varE = exp SEMIC {
					VarNode varNode = new VarNode($varID.text,$varT.ast,$varE.ast);   //crea il nodo variabile

					
					if (symbolTableMethodLevel.put($varID.text,new STentry(nestingLevel,$varT.ast,offset--, false)) != null  ) { //inserisce la variabile
			         	System.out.println("Var "+$varID.text+" already declared at line " + $varID.line);
			            stErrors++;
					}
					
					methodNode.addDec( varNode );
				} )+ IN {
					//sto tornando nello scope della classe, quindi rimetto l'offset pari a quello della classe
					offset = oldOffset; 
				}
		    )?
		    //aggiunge il corpo del metodo
		    e = exp { 
		    	methodNode.addBody( $e.ast );	      	            
		       	symTable.remove( nestingLevel-- ); //rimuove la hashmap corrente poiche' esce dallo scope 
		       	  									//del metodo
		    } SEMIC
		)* 
		CRPAR { 
			$astList.add(classNode); //aggiunge la classe alla lista delle classi
			
			//prende ogni metodo, lo ordina in base all'offset e setta il metodo nel ClassTypeNode
			virtualTable.entrySet().stream().filter( e -> e.getValue().isMethod() ).sorted( (e1, e2) -> e1.getValue().getOffset() - e2.getValue().getOffset())
					.forEach(e -> classTypeNode.setMethod(e.getValue().getType()));
			
			//stessa cosa di sopra coi campi
			virtualTable.entrySet().stream().filter(e -> !e.getValue().isMethod() ).sorted((e1, e2) -> e2.getValue().getOffset() - e1.getValue().getOffset()).forEach(e -> {
				FieldNode f = new FieldNode(e.getKey(), e.getValue().getType());
				f.setOffset(e.getValue().getOffset());
				classTypeNode.setField(f);
			} );
			
			//Aggiunge alla classTable una nuova entry che mappa la Virtual Table appena creata
			classTable.put( $clsID.text, virtualTable );
			
			//rimuove la hashmap della classe poiche' sta uscendo dallo scope della dichiarazione
			symTable.remove( nestingLevel--);
			

			
				
		}
    )+ ;

/*
 * LISTA DI DICHIARAZIONI
 * Lista per la dichiarazione di variabili e funzioni.
 * offset inizia da offset delle classi solo per il nestingLevel 0.
 */

declist	returns [List<Node> astList] :
	{ $astList= new ArrayList<Node>( ); }   //crea la lista di dichiarazioni   
	  ((
	  	/*
	  	 * dichiara la variabile con tipo hottype perche' e' piu' generico di type e gli assegna 
	  	 * un'espressione
	  	 */  										
		VAR varID=ID COLON ht=hotype ASS e=exp{
		Map<String,STentry> symbolTableLevel = symTable.get(nestingLevel); //prende la tabella a questo nestingLevel
			
        VarNode varNode = new VarNode($varID.text,$ht.ast,$e.ast);   //crea il nodo variabile
        $astList.add(varNode);   //lo aggiunge alla lista di dichiarazioni
             /*
              * Se e'' di tipo funzionale l'offset viene decrementato due volte.
              * Permette di memorizzare sia l'indirizzo della funzione sia il Frame Pointer a questo Activation Record.
              */
        	if($ht.ast instanceof ArrowTypeNode){
            	offset--;
          	}

         	
            if ( symbolTableLevel.put($varID.text,new STentry(nestingLevel,$ht.ast,offset--, false)) != null  ) { //inserisce la variabile
            	System.out.println("Var "+$varID.text+" already declared at line "+$varID.line);
              	stErrors++; 
            }  
        }
	|  
		FUN funID=ID COLON t=type { //dichiarazione di funzione 
			//Recupera la tabella del nestingLevel corrente.
          	Map<String,STentry> symbolTableLevel = symTable.get(nestingLevel);
          	
          	List<Node> parTypes = new ArrayList<Node>(); //crea la lista dei parametri della funzione
			                         
			FunNode funNode = new FunNode($funID.text,$t.ast); //crea il nuovo nodo funzione
          	$astList.add(funNode); //lo aggiunge alla lista delle dichiarazione
          				
          	//Inserisce nella tabella la funzione che inizialmente ha lista dei parametri vuota
            if (symbolTableLevel.put($funID.text,new STentry(nestingLevel,new ArrowTypeNode(parTypes,$t.ast),offset--, false)) != null) {
               	System.out.println("Fun "+$funID.text+"  already declared at line "+$funID.line);
                stErrors++; 
            }
                
            /*
             * Se e' di tipo funzionale l'offset viene decrementato due volte.
             * Permette di memorizzare sia l'indirizzo della funzione sia il Frame Pointer a questo Activation Record.
             */
           	offset--;  
                
            /*
             * crea una nuova HashMap per la Symbol Table e incrementa il nestingLevel 
             * perche' sta andando nello scope dei parametri della funzione.
             */ 
          
            Map<String,STentry> symbolTableParLevel = new HashMap<String,STentry> ();
            symTable.add(symbolTableParLevel);
            nestingLevel++;
            
        }
        
        LPAR {int parOffset=1;}
			(parID1=ID COLON parT1=hotype { //primo parametro, hottype perche' e' piu' generico
				
                parTypes.add($parT1.ast); //aggiunge il parametro alla lista dei parametri
                ParNode parNode = new ParNode($parID1.text,$parT1.ast); //crea nodo ParNode
                if($parT1.ast instanceof ArrowTypeNode)  // Se di tipo funzionale incrementa due volte l'offset
               		parOffset++;
                funNode.addPar(parNode); //lo attacco al FunNode con addPar
                if ( symbolTableParLevel.put($parID1.text,new STentry(nestingLevel,$parT1.ast,parOffset++, false )) != null ) { //aggiunge la dichiarazione a symbolTableParLevel
                	System.out.println("Parameter id "+$parID1.text+" already declared at line "+$parID1.line);
                   	stErrors++; 
                }
            }
            (COMMA parIDn=ID COLON parTn=hotype {
            	parTypes.add($parTn.ast); //aggiunge il parametro alla lista dei parametri
                ParNode parNodeN = new ParNode($parIDn.text,$parTn.ast); //crea nodo ParNode
                if($parTn.ast instanceof ArrowTypeNode) // Se di tipo funzionale incrementa due volte l'offset
		        	parOffset++;
                funNode.addPar(parNodeN);
                if ( symbolTableParLevel.put($parIDn.text,new STentry(nestingLevel,$parTn.ast,parOffset++, false )) != null ) { //aggiunge la dichiarazione a symbolTableParLevel
                	System.out.println("Parameter id "+$parIDn.text+" already declared at line "+$parIDn.line);
                    stErrors++; 
                }
            }
        )*
    )? 
    RPAR(
    		{ 
    			//entra nello scope di dichiarazione della funzione, quindi deve ripristinare l'offset
				int oldOffset = offset;
				offset = -2;
			}
		
			LET d = declist IN  {
				funNode.addDec( $d.astList ); 
				//entra nello scope del corpo quindi l'offset e' rimesso a quello della funzione
				offset = oldOffset;
			}
        )? e=exp {//crea il corpo della funzione
        	funNode.addBody($e.ast); //lo aggiunge alla funzione
            //rimuove la HashMap al nestingLevel corrente poiche' esce dallo scope               
            symTable.remove(nestingLevel--);    
        }
    	) SEMIC
	)+ ;

/*
 * COMPONENTI DEL PROGRAMMA
 */

/*
 * Tipo hottype e'il piu' generico ed e' utilizzato come nodo di smistamento per type e arrow.
 */

hotype  returns [Node ast]:
		t=type {$ast=$t.ast;}
  	|	a=arrow {$ast=$a.ast;} ;
	
//Tipo non funzionale. Puo' ritornare un IntTypeNode, un BoolTypeNode o un RefTypeNode
type	returns [Node ast] :
		INT  {$ast = new IntTypeNode();}
	|	BOOL {$ast = new BoolTypeNode();} 
	| 	i = ID  {$ast = new RefTypeNode($i.text); } ;	

/*
 * Tipo funzionale, e' una funzione che prende piu' parametri con tipi diversi e ritorna un unico tipo
 * che non puo' essere funzionale.
 */
arrow 	returns [Node ast] :
	LPAR { List<Node> parList = new ArrayList<>(); }
	(h1=hotype { parList.add($h1.ast);} 
	(COMMA hn=hotype { parList.add($hn.ast);}
	)* )? RPAR ARROW t=type {
		$ast = new ArrowTypeNode(parList, $t.ast);
	} ;  

/*
 * Espressione exp: e' una composizione di uno o piu' termini term mediante +, - e OR.
 * Termine term: e' una composizione di uno o pi� fattori factor mediante *, / e AND.
 * Fattori factor: sono identificatori o espressioni racchiuse tra parentesi ==, <=, >=
 * 
 * Per arrivare da value a exp, posso avere in mezzo tutte le operazioni descritte, ma posso
 * anche non averle.
 */

//Rappresenta un'espressione. Fa il parsing di operazioni di somma, sottrazione e OR.
exp		returns [Node ast] :
	t=term { $ast= $t.ast; }
	(
 		PLUS l=term {$ast= new PlusNode ($ast,$l.ast);}
 	| 	MINUS l=term {$ast= new MinusNode($ast,$l.ast);}
 	|	OR l=term {$ast= new OrNode($ast,$l.ast);}
 	)* ;
 
 /*
  * Rappresenta un'espressione con maggiore precedenza rispetto a exp. Infatti esegue il parsing per la 
  *	moltiplicazione, divisione e AND.
  */	

term	returns [Node ast] :
	f=factor {$ast= $f.ast;}
	(
		TIMES l=factor {$ast= new TimesNode ($ast,$l.ast);}
	|	DIV l=factor {$ast= new DivNode ($ast,$l.ast);} 
	|	AND l=factor {$ast= new AndNode ($ast,$l.ast);}
	)* ;

/*
 * Rappresenta operazioni di confronto. Esegue il parsing del nodo uguaglianza, maggiore uguale e minore uguale.
 */
factor	returns [Node ast] :
	v=value {$ast= $v.ast;}
	(
		EQ l=value {$ast= new EqualNode ($ast,$l.ast);}
	|	GE l = value {$ast= new GreaterEqualNode ($ast,$l.ast);}
	|	LE l = value {$ast= new LessEqualNode ($ast,$l.ast);} 
	)* ;	 	
 
/*
 * VALORI
 */
value	returns [Node ast] :
		n=INTEGER {$ast= new IntNode(Integer.parseInt($n.text));}  
	| 	TRUE {$ast= new BoolNode(true);}  
	| 	FALSE {$ast= new BoolNode(false);}
	| 	NULL { $ast= new EmptyNode( ); }
	|	NEW clsID = ID {
	
			//prende la virtual table dalla classTable con l'id della classe
			Map<String, STentry> virtualTable = classTable.get($clsID.text); 
			//se non c'e' vuol dire che la classe non esiste		
			if ( virtualTable == null) {
				System.out.println("Class " + $clsID.text + " not found at line " + $clsID.line);
	      		stErrors++;
			}
				
				//prende l'STEntry a livello 0 ovvero dove sono dichiarate tutte le classi
			NewNode newNode = new NewNode(symTable.get(0).get($clsID.text), $clsID.text); //crea un newNode
				
		}
		LPAR ( //e' il costruttore a cui vengono passati i parametri
			e1 = exp {
				newNode.setField( $e1.ast );
			}
		(COMMA en = exp { 
			newNode.setField( $en.ast );
		})*
		)? RPAR { $ast = newNode; }
	
	| 	LPAR e=exp RPAR {$ast= $e.ast;}  //espressione tra parentesi
	| 	IF x=exp THEN CLPAR y=exp CRPAR  //espressione if then else
		ELSE CLPAR z=exp CRPAR
	  		{$ast= new IfNode($x.ast,$y.ast,$z.ast);}	
	|	NOT LPAR e=exp RPAR {$ast= new NotNode($e.ast);} //not
	|	PRINT LPAR e=exp RPAR {$ast= new PrintNode($e.ast);} //print
	
	/*
	 * Se non e' seguito da parentesi o punto e' solo un ID, se e' seguito da tonde e' una chiamata a 
	 * funzione, se e' seguito da punto e' una chiamata a metodo.
	 */
	| 	i = ID {
			Map<String,STentry> symbolTableLevel = symTable.get(0); //prende la HashMap a livello 0
		
        	int nl=nestingLevel;
           	STentry entry=null;
           	while (nl>=0 && entry==null) //risale il nesting level fino al nestingLevel a cui e' dichiarata
           		entry=(symTable.get(nl--)).get($i.text);

			//se non lo trova non e' dichiarato l'ID
           	if (entry==null) { 
             	System.out.println("Id "+$i.text+" undeclared at line "+$i.line);
             	stErrors++; 
            }
            //altrimenti crea un nuovo IdNode
            $ast = new IdNode($i.text, entry, nestingLevel);
	   	}
	   	//se c'e' una parentesi dopo l'ID e' una funzione, quindi gli deve passare gli argomenti
	   	( LPAR {List<Node> parList = new ArrayList<Node>();}
		   	( par1 = exp { parList.add( $par1.ast ); }
				( COMMA parN = exp { parList.add( $parN.ast ); } )*
		   	)? RPAR { $ast = new CallNode( $i.text, entry, parList, nestingLevel ); } //crea un nuovo CallNode
		
		//altrimenti se c'e' il punto e' una invocazione di metodo
		|	DOT metID = ID {
				RefTypeNode reference = ( RefTypeNode ) entry.getType(); //prende il tipo dell'entry
				
				
				//prende la classEntry dalla symbol table con id uguale al RefTypeNode
				STentry classEntry = symTable.get(0).get(reference.getID()); 
				
				//controlla se classEntry non e' null
				if(classEntry == null){
					System.out.println("Class "+ reference.getID() + " undeclared at line "+$metID.line);
             		stErrors++;
				}
				
				//prende il methodEntry dalla class table con id uguale al RefTypeNode e ID uguale al metodo
				STentry methodEntry = classTable.get(reference.getID()).get($metID.text); 
				
				//controlla se methodEntry non e' null
				if(methodEntry == null){
					System.out.println("Method "+ $metID.text + " undeclared at line "+$metID.line);
             		stErrors++;
				}
				else if(!methodEntry.isMethod()){
					System.out.println("Id "+ $metID.text + " is not a method at line "+$metID.line);
             		stErrors++;
				}

				//istanzia la nuova chiamata di metodo
				ClassCallNode classCallNode = new ClassCallNode( entry, $metID.text, methodEntry, nestingLevel );
				$ast = classCallNode;
			}
			
			//parametri del metodo
			LPAR (
				met1 = exp { classCallNode.addPar( $met1.ast );	}
				(COMMA metN = exp {classCallNode.addPar($metN.ast);})*
			)? RPAR
		)? ;

/*------------------------------------------------------------------
 * LEXER RULES
 *------------------------------------------------------------------*/

PLUS  	: '+' ;
MINUS   : '-' ;
TIMES   : '*' ;
DIV 	: '/' ;
LPAR	: '(' ;
RPAR	: ')' ;
CLPAR	: '{' ;
CRPAR	: '}' ;
SEMIC 	: ';' ;
COLON   : ':' ; 
COMMA	: ',' ;
DOT	    : '.' ; 
OR	    : '||';
AND	    : '&&'; 
NOT	    : '!' ;
GE	    : '>=' ; 
LE	    : '<=' ; 
EQ	    : '==' ;	
ASS	    : '=' ;
TRUE	: 'true' ;
FALSE	: 'false' ;
IF	    : 'if' ;
THEN	: 'then';
ELSE	: 'else' ;
PRINT	: 'print' ;
LET     : 'let' ;	
IN      : 'in' ;	
VAR     : 'var' ;
FUN	    : 'fun' ; 
CLASS	: 'class' ;  
EXTENDS : 'extends' ;	
NEW 	: 'new' ;	
NULL    : 'null' ;	 
INT	    : 'int' ;
BOOL	: 'bool' ;
ARROW   : '->' ; 	
INTEGER : '0' | ('-')?(('1'..'9')('0'..'9')*) ; 
 
ID 	    : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')* ; 
 
WHITESP : (' '|'\t'|'\n'|'\r')+ -> channel(HIDDEN) ;

COMMENT : '/*' (.)*? '*/' -> channel(HIDDEN) ;

ERR     : . { System.out.println("Invalid char: "+ getText()); lexicalErrors++; } -> channel(HIDDEN); 
