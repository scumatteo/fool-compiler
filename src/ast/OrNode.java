package ast;

import lib.FOOLlib;
import lib.TypeException;

//Nodo per il tipo Or.
public class OrNode implements Node {

	private Node left; //sottoalbero di sinistra
	private Node right; //sottoalbero di destra

	public OrNode (Node l, Node r) {
		this.left=l;
		this.right=r;
	}

	public String toPrint(String s) {
		return s+"Or\n" + this.left.toPrint(s+"  ")  
		+ this.right.toPrint(s+"  ") ; 
	}
	
	/*
	 * Permette il confronto solo tra tipi booleani. Se almeno uno dei due non lo e' tira un'eccezione.
	 * Altrimenti ritorna un BoolTypeNode
	 */
	public Node typeCheck() throws TypeException {
		Node l= this.left.typeCheck();  
		Node r= this.right.typeCheck();  
		if (!(l instanceof BoolTypeNode && r instanceof BoolTypeNode))
			throw new TypeException("Incompatible types in or");
		return new BoolTypeNode();
	}
	
	/* 
	 * Per verificare se l'or e' true somma i due valori booleani e li confronta con 0.
	 * Se la somma e' 0 allora torna false (caso 0 + 0) altrimenti torna true casi (1+0, 0+1, 1+1)
	 * 
	 * La logica e' invertita rispetto all'AND.
	 */
	
	public String codeGeneration() {
		String l1= FOOLlib.freshLabel();
	    String l2= FOOLlib.freshLabel();
		  return this.left.codeGeneration()+
				 this.right.codeGeneration()+
				 "add\n"+
				 "push 0\n"+
				 "beq "+l1+"\n"+
				 "push 1\n"+
				 "b "+l2+"\n"+
				 l1+": \n"+
				 "push 0\n"+
				 l2+": \n";	   
	}

}
