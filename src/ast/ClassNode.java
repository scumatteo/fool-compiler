package ast;

import java.util.ArrayList;
import java.util.List;

import lib.FOOLlib;
import lib.TypeException;


public class ClassNode implements DecNode, Node {

	private STentry superEntry;
	private Node symType;  //ClassTypeNode
	private List<Node> listAllFields; // lista di tutti i campi
	private List<Node> listAllMethods; // lista di tutti i metodi (ArrowTypeNode)
	private String id;
	
	public ClassNode( Node clsType, String name ) {
		this.symType = clsType;
		this.superEntry = null;
		this.id = name;
		this.listAllFields = new ArrayList<>();
		this.listAllMethods = new ArrayList<>();
	}
	
	public void setSuper( STentry superEntry ) {
		this.superEntry = superEntry;
	}
	
	public STentry getSuper() {
		return this.superEntry;
	}
		
	public void setField( Node field ) {
		this.listAllFields.add( field );
	}
	
	public void setAllFields(List<Node> allFields) {
		this.listAllFields = allFields;
	}

	public List<Node> getAllFields( ) {
		return listAllFields;
	}
	
	public void setMethod( Node method ) {
		this.listAllMethods.add( method );
	}
	
	public void setAllMethods(List<Node> allMethods) {
		this.listAllMethods = allMethods;
	}
	
	public List<Node> getAllMethods( ) {
		return listAllMethods;
	}
	
	@Override
	public String toPrint(String s) {
		String decListStr = "";
		String fieldListStr = "";
			
		for (Node m : listAllMethods)
			decListStr += m.toPrint(s + "  ");
		
		for (Node f : listAllFields)
			fieldListStr += f.toPrint(s + "  ");
			
		return s + "Class: " + this.id + "\n" + fieldListStr + decListStr;
	}

	@Override
	public Node typeCheck() throws TypeException {
		//per ogni metodo della classe esegue il typecheck sul metodo
		for (Node m : listAllMethods)
			try {
				m.typeCheck();
			} catch (TypeException e) {
				System.out.println("Type checking error in a method: " + e.text);
			}
		
		
		if( superEntry != null ) { // check overriding
			//prende rispettivamente il ClassTypeNode della classe padre e della sua classe
			ClassTypeNode superCTN = (ClassTypeNode) superEntry.getType();
			ClassTypeNode thisCTN = ( ClassTypeNode ) symType;
			
			
			for ( Node f : listAllFields ) {
				int fieldOffset = -((FieldNode)f).getOffset() - 1; //prende l'offset di ogni campo

				//se l'offset e' minore della size della lista di tutti i campi del padre allora 
				//e' un override del campo
				if (fieldOffset < superCTN.getAllFields().size()) { // override
					FieldNode superField = (FieldNode) superCTN.getAllFields().get( fieldOffset );
					FieldNode myField = (FieldNode) thisCTN.getAllFields().get( fieldOffset );

					//e controlla che sia sottotipo del campo del padre
					if (!FOOLlib.isSubtype( myField, superField ))
						throw new TypeException("Overriding of field '" + superField.getId( ) + "' has wrong type. '");
				}
			}
			
			//fa la stessa cosa con i metodi: controlla l'override (non c'e' bisogno di calcolare l'offset)
			//e controlla se e' sottotipo di quello dichiarato nel padre
			for ( Node m : listAllMethods ) {
				MethodNode method = ( MethodNode ) m;

				if ( method.getOffset() < superCTN.getAllMethods().size()) { // override
					ArrowTypeNode superMethod = (ArrowTypeNode) superCTN.getAllMethods().get(method.getOffset());
					ArrowTypeNode myMethod = (ArrowTypeNode) thisCTN.getAllMethods().get(method.getOffset());

					if ( ! FOOLlib.isSubtype( myMethod, superMethod ) )
						throw new TypeException("Overriding of method '" + method.getID( ) + "' has wrong type" );
				}
			}
		}
		
		return null;
	}

	@Override
	public String codeGeneration() {
		List<String> myDispatchTable = new ArrayList<>(); //crea una nuova dispatch table
		FOOLlib.dispatchTable.add(myDispatchTable); //la aggiunge e la andremo a riempire in seguito
		String label = ""; 
		int labelOffset;
		int sizeMethod = 0; //0 perche' all'inizio non ha metodi
		if ( superEntry != null ) { //se e' sottotipo di un altra classe
			//allora la sizeMethod viene settata alla size della superclasse, perche' eredita i metodi
			//della superclasse
			sizeMethod = ((ClassTypeNode)(this.superEntry.getType())).getAllMethods().size();
			//prendo le label della superclasse
			List<String> superLabel = FOOLlib.dispatchTable.get(-superEntry.getOffset()-2); 
			
			//per ogni label della superclasse la aggiunge alla dispacth table della sottoclasse
			for(String s : superLabel) {
				myDispatchTable.add(s);
			};
		}
		
		String labelList = "";
		
		for(Node m : this.listAllMethods) {
			//per ogni metodo fa la code generation e setta la label alla label del metodo e l'offset
			//all'offset del metodo
			((MethodNode)m).codeGeneration();
			label = ((MethodNode)m).getLabel();
			labelOffset = ((MethodNode)m).getOffset();
			
			//se l'offset e' minore della size della lista dei metodi allora sta facendo ovverride
			if(labelOffset < sizeMethod) { //check override
				myDispatchTable.remove(labelOffset); //rimuove la vecchia label
				myDispatchTable.add(labelOffset, label); //inserisce la nuova label all'indice labelOffset
			} else {
				myDispatchTable.add(label);	//altrimenti aggiunge semplicemente la label
			}
		}
		
		//Per ogni etichetta
		for (String s:myDispatchTable) {
			labelList += "push " + s + "\n"  //push label
						+ "lhp\n" //memorizzo l'etichetta in hp
						+ "sw\n" 
						+ "push 1\n" + "lhp\n" + "add\n" + "shp\n"; //hp++
		}

		return "lhp\n" //object pointer da ritornare alla fine
				+ labelList;
	}

	@Override
	public Node getSymType() {
		return symType;
	}

}
