package ast;

import java.util.ArrayList;
import java.util.List;

import lib.FOOLlib;
import lib.TypeException;

//Nodo che implementa la chiamata a metodo da fuori della classe.
public class ClassCallNode implements Node {

	private int nestingLevel; // livello di nesting a cui e' stato chiamato il metodo
	private STentry entry; // Le informazioni riguardo alla classe (tipo, offset) inserite in symbol table
	private STentry methodEntry; // Metodo che si sta chiamando
	private List<Node> parList; // lista di parametri passati
	private String methodId; // id del metodo

	public ClassCallNode(STentry entry, String mId, STentry mtdEntry, int nl) {
		this.entry = entry;
		this.methodEntry = mtdEntry;
		this.nestingLevel = nl;
		parList = new ArrayList<Node>();
		this.methodId = mId;
	}

	// Permette di aggiungere un parametro alla lista dei parametri
	public void addPar(Node parameter) {
		parList.add(parameter);
	}

	// Ritorna il tipo messo in symbol table
	public Node getType() {
		return methodEntry.getType();
	}

	@Override
	public String toPrint(String s) {
		return s + "Class call of method: '" + methodId + "'\n" + s + methodEntry.getType().toPrint(s + "  ");
	}

	
	/*
	 * Controlla che sia ArrowTypeNode, se non lo e' lancia un'eccezione.
	 * Controlla la coerenza dei parametri (numero e tipo). I parametri possono essere solo o variabili
	 * o dichiarazioni o chiamate a metodo/funzione o un ArrowTypeNode. Controllo se i tipi sono coerenti
	 * e se non lo sono tiro un'eccezione.
	 * 
	 */
	@Override
	public Node typeCheck() throws TypeException {
		if (!(methodEntry.getType() instanceof ArrowTypeNode)) {
			throw new TypeException("Invocation of a non-method " + this.methodId);
		}

		ArrowTypeNode arrowNode = (ArrowTypeNode) methodEntry.getType();
		List<Node> parList = arrowNode.getParList();
		if (!(parList.size() == this.parList.size()))
			throw new TypeException(
					"Wrong number of parameters in the invocation of method " + this.methodId);

		for (int i = 0; i < this.parList.size(); i++) {
			Node par = this.parList.get(i);
			if (par instanceof IdNode)
				par = ((IdNode) par).getEntry().getType();
			else if (par instanceof DecNode)
				par = ((DecNode) par).getSymType();
			else if (par instanceof CallNode)
				par = ((CallNode) par).getType();
			else if (par instanceof ClassCallNode)
				par = ((ClassCallNode) par).getType();
			else
				par = par.typeCheck();

			if (par instanceof ArrowTypeNode)
				par = ((ArrowTypeNode) par).getRet();

			if (!(FOOLlib.isSubtype(par, ((ParNode) parList.get(i)).getSymType())))
				throw new TypeException("Wrong type of parameter in invocation of method");
		}

		return arrowNode.getRet();
	}

	@Override
	public String codeGeneration() {
		String parCode = "";
		String getAR = "";
		
		for (int i = parList.size() - 1; i >= 0; i--)
			parCode += parList.get(i).codeGeneration();
		
		for (int i = 0; i < nestingLevel - entry.getNestingLevel(); i++)
			getAR += "lw\n";

		return "lfp\n"+ //pusha il control link (punta al frame del chiamante)
		         parCode+ //genera codice per i parametri in ordine inverso.
		         "lfp\n"+ //ripusha il control link
	             getAR+ //pusha l'access link (punta al frame di dichiarazione di funzione) risalendo la catena statica
				"push " + entry.getOffset() + "\n" + // pusha l'offset dell'object pointer
				"add\n" + // con la add ottiene l'object pointer dell'oggetto nell'AR
				"lw\n" + // carica sullo stack il contenuto puntato dall'object pointer
				"stm\n" + "ltm\n" + "ltm\n" + // duplica il valore sullo stack
				"lw\n" + // mette l'indirizzo della dispatch table sullo stack
				"push " + methodEntry.getOffset() + "\n" +//pusha l'offset del metodo 
				"add\n" + //con la add ottiene l'indirizzo del metodo nella dipatch table
				"lw\n" + // carica sullo stack
				"js\n"; // salta all'indirizzo poppato (mettendo in $ra l'indirizzo dell'istruzione successiva)
	}

}
