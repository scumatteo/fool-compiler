package ast;

import lib.*;

// Nodo di tipo equals ==
public class EqualNode implements Node {

    private Node left;  // sottoalbero di sinistra
    private Node right; // sottoalbero di destra

    public EqualNode(final Node l, final Node r) {
        this.left = l;
        this.right = r;
    }

    public String toPrint(final String s) {
        return s + "Equal\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
    }

    /*
     * Controlla se sono nodi di tipo ArrowType. Se lo sono lancia un'eccezione.
     * Dovrebbe confrontare coppie di valori (oltre al tipo di ritorno anche i
     * parametri). Confronta se sono sottotipi i due sottoalberi. Se non lo sono
     * lancia un'eccezione. Altrimenti restituisce un nodo di tipo bool.
     */
    public Node typeCheck() throws TypeException {
        Node l = left.typeCheck();
        Node r = right.typeCheck();
        if (l instanceof ArrowTypeNode || r instanceof ArrowTypeNode) {
            throw new TypeException("Incompatible types in equal");
        }
        if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l)))
            throw new TypeException("Incompatible types in equal");
        return new BoolTypeNode();
    }

    public String codeGeneration() {
	String l1 = FOOLlib.freshLabel();
	String l2 = FOOLlib.freshLabel();
	return left.codeGeneration() + 
	       right.codeGeneration() + 
	       "beq " + l1 + "\n" +    // se sono uguali salta a l1 dove pusha 1 (true)
	       "push 0\n" +            // altrimenti pusha 0 (false)
	       "b " + l2 + "\n" +      // e salta direttamente a l2
	       l1 + ": \n" +           // se sono uguali arriva qui
	       "push 1\n" +            // e pusha 1 True
	       l2 + ": \n";
	}

}