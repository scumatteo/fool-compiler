package ast;

//Nodo di tipo Bool.
public class BoolTypeNode implements Node {
  
  public BoolTypeNode () {
  }
  
  public String toPrint(String s) {
   return s+"BoolType\n";  
  }
  
  //typeCheck non utilizzato
  public Node typeCheck() {
	  return null;
  }
 
  //codeGeneration non utilizzato
  public String codeGeneration() {
	  return "";
  }

}  