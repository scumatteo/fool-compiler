package ast;

import lib.*;

//Nodo per il tipo moltiplicazione *
public class TimesNode implements Node {

	private Node left; // sottoalbero di sinistra
	private Node right; // sottoalbero di destra

	public TimesNode(Node l, Node r) {
		this.left = l;
		this.right = r;
	}

	public String toPrint(String s) {
		return s + "Times\n" + this.left.toPrint(s + "  ") + this.right.toPrint(s + "  ");
	}

	/*
	 * Se non sono entrambi degli interi tira un'eccezione, altrimenti ritorna un
	 * IntTypeNode
	 */
	public Node typeCheck() throws TypeException {
		if (!(FOOLlib.isSubtype(this.left.typeCheck(), new IntTypeNode())
				&& FOOLlib.isSubtype(this.right.typeCheck(), new IntTypeNode())))
			throw new TypeException("Non integers in multiplication");
		return new IntTypeNode();
	}

	// Effettua una semplice mult della SVM.
	public String codeGeneration() {
		return this.left.codeGeneration() + this.right.codeGeneration() + "mult\n";
	}

}