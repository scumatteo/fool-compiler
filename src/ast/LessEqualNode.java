package ast;

import lib.*;

//Nodo per il tipo "minore-uguale"
public class LessEqualNode implements Node {

	private Node left; // Sottoalbero di sinistra
	private Node right; // Sottoalbero di destra

	public LessEqualNode( Node l, Node r ) {
		left = l;
		right = r;
	}

	public String toPrint( String s ) {
		return s + "LessEqual\n" + left.toPrint( s + "  " ) + right.toPrint( s + "  " );
	}

	/*
	 * Controlla che i due nodi siano uno sottotipo dell'altro.
	 * Se non lo sono tira un'eccezione.
	 */
	public Node typeCheck() throws TypeException {
		Node l = left.typeCheck( );				// type-check sull'espressione/valore di sinistra
		Node r = right.typeCheck( );			// type-check sull'espressione/valore di destra
		
		// se nessuno dei due e' sottotipo dell'altro il confronto non e' effettuabile
		if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l)))
			throw new TypeException("Incompatible types in less equal");
		return new BoolTypeNode( );				// l'espressione <= fornisce come risposta/risultato un booleano
	}

	public String codeGeneration( ) {
		String lessEqual = FOOLlib.freshLabel( );
		String greater = FOOLlib.freshLabel( );
		return left.codeGeneration( ) +			// prendo il valore di sinistra
				right.codeGeneration( ) + 		// prendo il valore di destra
				"bleq " + lessEqual + "\n" + 	// se left <= right, salto a lessEqual
				"push 0\n" + 					// in caso negativo pusho 0 (false)
				"b " + greater + "\n" +			// e salto direttamente a greater (fine del confronto)
				lessEqual + ": \n" +			// se e' <=, arrivo qui
				"push 1\n" + 					// e pusho 1 (true)
				greater + ": \n";				// fine del confronto
	}

}