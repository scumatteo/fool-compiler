package ast;

import java.util.List;

import lib.*;

//Nodo di chiamata a funzione o metodo se si e' dentro la classe.
public class CallNode implements Node {

	private String id; //Nome della funzione o metodo
	private int nestingLevel; //livello di nesting a cui e' stata chiamata
	private STentry entry; //Le informazioni riguardo al nodo (tipo, offset) inserite in symbol table
	private List<Node> parList; //lista dei parametri

	public CallNode(String i, STentry stEntry, List<Node> parList, int nl) {
		id = i;
		entry = stEntry;
		this.parList = parList;
		nestingLevel = nl;
	}
	
	//Ritorna il tipo messo in symbol table
	public Node getType() {
		return entry.getType( );
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parList)
			parlstr += par.toPrint(s + "    ");
		return s + "Call: " + id + "\n" + entry.toPrint(s + "  ") + s + "  pars:\n" + parlstr;
	}

	/*
	 * Se non e' di tipo Arrow tira un'eccezione. Invece se e' di tipo Arrow controlla che i parametri passati
	 * siano coerenti con quelli di ArrowType (numero e sottotipo).
	 * Ritorna il tipo di ritorno messo in symbol table.
	 * 
	 */
	public Node typeCheck() throws TypeException {
		if (!(entry.getType() instanceof ArrowTypeNode))
			throw new TypeException("Invocation of a non-function " + id);
		ArrowTypeNode t = (ArrowTypeNode) entry.getType();
		List<Node> p = t.getParList();
		if (!(p.size() == parList.size()))
			throw new TypeException("Wrong number of parameters in the invocation of " + id);
		for (int i = 0; i < parList.size(); i++)
			if (!(FOOLlib.isSubtype(parList.get(i).typeCheck(), p.get(i)))) {
				throw new TypeException("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + id);
			}
		return t.getRet();
	}


	public String codeGeneration() {
		String parCode = "";
		String getAR = "";
		
		for (int i = parList.size() - 1; i >= 0; i--)
			parCode += parList.get(i).codeGeneration(); //per ogni parametro viene fatta la code generation in ordine inverso
		
		for (int i = 0; i < nestingLevel - entry.getNestingLevel(); i++)
			getAR += "lw\n"; //risale la catena statica fino all'indirizzo dell'AR
		
		if(entry.isMethod()) {
			return  "lfp\n"+ //pusha il control link (punta al frame del chiamante)
			         parCode+ //genera codice per i parametri in ordine inverso.
			         "lfp\n"+ //ripusha il control link
		             getAR+ //pusha l'access link (punta al frame di dichiarazione di funzione) risalendo la catena statica
			         "stm\n"+"ltm\n"+"ltm\n"+ //duplica il top dello stack
			         "lw\n" + //indirizzo della dispatch table (dispatch pointer)
			         "push "+ entry.getOffset() + "\n"+ //pusha l'offset del metodo nella DT
					 "add\n"+ //facendo la add ottiene l'indirizzo del metodo
		             "lw\n"+ // pusha il valore all'indirizzo del metodo (punta al frame di dichiarazione 
					 		// del metodo + il suo offset)
			         "js\n";// salta all'indirizzo poppato (mettendo in $ra l'indirizzo dell'istruzione successiva)
		} else {
			return "lfp\n" + //pusha il Control Link (punta al frame della funzione id chiamante)
					parCode + //genera codice per i parametri in ordine inverso.
					"lfp\n" + //ripusha il control link
					getAR + //pusha l'access link (punta al frame di dichiarazione di funzione) risalendo la catena statica
					"push " + entry.getOffset() + "\n" + //pusha l'offset della funzione nell'AR dov'e' dichiarata
					"add\n" + //facendo la add ottiene l'indirizzo della funzione
					"stm\n" + "ltm\n" + //copio il top dello stack
					"lw\n" + //pusha il valore che c'e' all'indirizzo in cima allo stack cioe' indirizzo AR del chiamante
					"ltm\n" + // ripusho l'indirizzo ottenuto precedentemente, per poi calcolarmi offset - 1
					"push 1\n" + // push 1, 
					"sub\n" + // sottraggo a offset 1, per recuperare l'indirizzo funzione.
					"lw\n" + // pusha l'indirizzo della funzione
					"js\n"; // salta all'indirizzo poppato (mettendo in $ra l'indirizzo dell'istruzione successiva)
		}
	}
}