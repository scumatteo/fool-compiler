package ast;

import lib.*;
import java.util.List;

//Nodo per il tipo programma in cui c'e' let in, quindi definizione di variabili, classi, funzioni.
public class ProgLetInNode implements Node {

	private List<Node> decList; // dichiarazioni di variabili, classi, funzioni
	private Node exp; // espressione

	public ProgLetInNode(List<Node> d, Node e) {
		this.decList = d;
		this.exp = e;
	}

	public String toPrint(String s) {
		String declstr = "";		
		for (Node dec : this.decList)
			declstr += dec.toPrint(s + "  ");
		return s + "ProgLetIn\n" + declstr + this.exp.toPrint(s + "  ");
	}

	/*
	 * Per ogni dichiarazione controlla il tipo.
	 */
	public Node typeCheck() throws TypeException {
		for (Node dec : this.decList)
			try {
				dec.typeCheck();
			} catch (TypeException e) {
				System.out.println("Type checking error in a declaration: " + e.text);
			}
		return exp.typeCheck();
	}

	public String codeGeneration() {
		String declCode = "";
		for (Node dec : this.decList)
			declCode += dec.codeGeneration(); // crea il codice per ogni dichiarazione
		return "push 0\n" + // 0 e' l'istruzione iniziale
				declCode + // pusha il codice delle dichiarazioni in Let
				this.exp.codeGeneration() + // pusha il codice dell'espressione in In
				"halt\n" + // fine programma
				FOOLlib.getCode(); // ritorna il codice delle funzioni e metodi
	}
}