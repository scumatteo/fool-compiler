package ast;

// Interfaccia per FunNode, VarNode e ParNode
public interface DecNode {

    // Restituisce il tipo del valore inserito in Symbol Table
    Node getSymType();

}
