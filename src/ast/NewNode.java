package ast;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lib.FOOLlib;
import lib.TypeException;


//Nodo per il tipo "new".
public class NewNode implements Node {

	private STentry entry; //Le informazioni riguardo al nodo (tipo, offset) inserite in symbol table
	private List<Node> fieldList; //Lista dei parametri passati al costruttore
	private String id; //id della classe istanziata
	
	public NewNode(STentry entry, String id) {
		this.entry = entry;
		this.fieldList = new ArrayList<Node>( );
		this.id = id;
	}
	
	public List<Node> getFieldList(){
		return fieldList;
	}
	
	public void setField( Node field ) {
		fieldList.add( field );
	}

	@Override
	public String toPrint( String indent ) {
		return indent + "New class: " + id + "\n" +
				fieldList.stream().map( s -> s.toPrint( indent + "\t" )).collect( Collectors.joining());
	}

	/*
	 * Se il tipo messo in symbol table non e' di tipo classe allora lancia un'eccezione.
	 */
	@Override
	public Node typeCheck() throws TypeException {
		// Se il tipo messo in symbol table non e' di tipo classe allora lancia un'eccezione.
		if (!(entry.getType() instanceof ClassTypeNode))
			throw new TypeException("Invocation of a non-class " + id);
		
		ClassTypeNode ctn = (ClassTypeNode) entry.getType();	// prende il class-type dalla entry
		List<Node> requiredFields = ctn.getAllFields();			// e ne prende i figli

		// Controlla se il numero dei parametri passati al costruttore e' coerente.
		// Se non lo e' lancia un'eccezione.
		if (!(requiredFields.size() == fieldList.size()))
			throw new TypeException("Wrong number of parameters in the instantiation of " + id + " p.size" + 
					ctn.getAllFields().size() + " fieldSize:" + fieldList.size());
		
		/*
		 * Per ogni parametro controllo se e' un id, una dichiarazione, una chiamata a a funzione/metodo
		 * o un ArrowTypeNode.
		 * Se il tipo messo in symbol table non e' sottotipo del tipo del parametro lancio un'eccezione.
		 */
		
		for(int i = 0; i < fieldList.size(); i++) {
			Node field = fieldList.get(i);
			
			FieldNode fieldNode = (FieldNode)requiredFields.get(i);
			
			if ( field instanceof IdNode )
				field = ( ( IdNode ) field).getEntry( ).getType( );
			else if ( field instanceof CallNode )
				field = ( ( CallNode ) field ).getType( );
			else if ( field instanceof DecNode )
				field = ( ( DecNode ) field ).getSymType( );
			else if ( field instanceof ClassCallNode )
				field = ( ( ClassCallNode ) field ).getType( );
			else field = field.typeCheck( );

			if ( field instanceof ArrowTypeNode )
				field = ( ( ArrowTypeNode )field ).getRet( );
			
			if ( !( FOOLlib.isSubtype(field, fieldNode.getSymType()) ) ) {		
				throw new TypeException("Wrong type of fields " + fieldNode.getId( ) );
			}
		
		}
		return new RefTypeNode(this.id);
	}

	@Override
	public String codeGeneration() {
		
		// per ogni parametro fa push dei valori sullo stack
		String valList = fieldList.stream().map( Node::codeGeneration ).collect( Collectors.joining( ) );

		valList += fieldList.stream().map( e ->
			"lhp\n" +	//push hp. Mette sullo stack il primo indirizzo libero sullo heap
			"sw\n" +	//salva la variabile all'indirizzo dello heap.
			"push 1\n" + "lhp\n" + "add\n" + "shp\n"	//aggiorna l'heap (hp++)
		).collect( Collectors.joining( ) );

		//inizio dello stack a cui si aggiunge l'offset della classe. Si prende dall'AR del programma principale
		//l'indirizzo della dispatch table
		int dispatchTableStackOffset = entry.getOffset() + FOOLlib.MEMSIZE; 
		valList += "push " + dispatchTableStackOffset + "\n" + // pusha il Dispatch Pointer. 
										//prende l'AR globale in memsize e tramite l'offset prende l'indirizzo
										//della dispatch table
				"lw\n" +	//prende il valore del dispatch point
				"lhp\n" +	//mette hp sullo stack
				"sw\n" +	//mette l'indirizzo della dispatch table nell'object
				"lhp\n";	//mette hp sullo stack

		return valList + "push 1\n" + "lhp\n" + "add\n" + "shp\n"; //semplicemente un incremento.
	}

}
