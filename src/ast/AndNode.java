package ast;

import lib.TypeException;

//Rappresenta un nodo And a && b
public class AndNode implements Node {

	private Node left; //sottoalbero di sinistra
	private Node right; //sottoalbero di destra

	public AndNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	public String toPrint(String s) {
		return s + "And\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	/* Permette check solo tra tipi booleani. Se non lo sono, tira un'eccezione. */
	public Node typeCheck() throws TypeException {
		Node l = left.typeCheck();
		Node r = right.typeCheck();
		if (!(l instanceof BoolTypeNode && r instanceof BoolTypeNode))
			throw new TypeException("Incompatible types in and");
		return new BoolTypeNode();
	}

	/*
	 * Questo metodo restituisce il risultato della moltiplicazione tra i due
	 * elementi in input. Restituisce: 1 per True 0 per False.
	 */
	public String codeGeneration() {
		return left.codeGeneration() + 
				right.codeGeneration() + 
				"mult\n";
	}

}
