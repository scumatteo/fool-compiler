package ast;

import lib.*;

// Nodo di tipo if
public class IfNode implements Node {

    private Node cond;  // condizione
    private Node th;    // nodo then
    private Node el;    // nodo else

    public IfNode(final Node cond, final Node t, final Node e) {
        this.cond = cond;
        this.th = t;
        this.el = e;
    }

    public String toPrint(final String s) {
        return s + "If\n" + cond.toPrint(s + "  ") + s + "Then\n" + th.toPrint(s + "  ") + 
        		s + "Else\n" + el.toPrint(s + "  ");
    }

    /*
     * Se la condizione non e' sottotipo di BoolTypeNode lancia un'eccezione.
     * Altrimenti fa il type checking delle espressioni then e else e controlla che
     * siano una il sottotipo dell'altra, restituendo quest'ultimo tipo.
     * Controlla che le espressioni abbiano un sopratipo comune,
     * se cosi' non e' lancia un'eccezione, altrimenti restituisce il sopratipo.
     */
    public Node typeCheck() throws TypeException {
        if (!(FOOLlib.isSubtype(cond.typeCheck(), new BoolTypeNode())))
            throw new TypeException("Non boolean condition in if");
        Node t = th.typeCheck();
        Node e = el.typeCheck();
        if (FOOLlib.isSubtype(t, e))
            return e;
        if (FOOLlib.isSubtype(e, t))
            return t;

        Node n = FOOLlib.lowestCommonAncestor(t, e);
        if (n == null)
            throw new TypeException("Incompatible types in then-else branches");

        return n;
    }
  
    public String codeGeneration() {
        String l1 = FOOLlib.freshLabel();
        String l2 = FOOLlib.freshLabel();
        return cond.codeGeneration() +  // genera il codice per la condizione
               "push 1\n" +             // pusha 1
               "beq " + l1 + "\n" +     // se sono uguali significa che la condizione e' true (= 1) e salta a l1
               el.codeGeneration() +    // se non sono uguali genera il codice dell'else
               "b " + l2 + "\n" +       // e salta a l2
               l1 + ": \n" +            // se sono uguali arriva qui
               th.codeGeneration() +    // e genera il codice per lo then
               l2 + ": \n";
    }
}  