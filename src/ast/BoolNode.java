package ast;

//Nodo booleano
public class BoolNode implements Node {

	private boolean value; // valore in input

	public BoolNode(boolean val) {
		value = val;
	}

	public String toPrint(String s) {
		if (value)
			return s + "Bool:true\n";
		else
			return s + "Bool:false\n";
	}

	// Torna un nodo di tipo booleano
	public Node typeCheck() {
		return new BoolTypeNode();
	}

	// Essendo sottotipo di interi, torna 1 se True, 0 se False.
	public String codeGeneration() {
		return "push " + (value ? 1 : 0) + "\n";
	}

}