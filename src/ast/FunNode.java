package ast;

import java.util.ArrayList;
import java.util.List;

import lib.*;

// Nodo di tipo funzione
public class FunNode implements Node, DecNode {

    private String id;          // id della funzione
    private Node symType;       // tipo di ritorno (inserito in Symbol Table)
    private List<Node> parList; // lista dei parametri
    private List<Node> decList; // lista di dichiarazioni di variabili locali
    private Node exp;           // body della funzione

    public FunNode(final String id, final Node n) {
        this.id = id;
        this.symType = n;
        this.parList = new ArrayList<Node>();
        this.decList = new ArrayList<Node>();
    }

    public void addDec(List<Node> dec) {
        this.decList = dec;
    }

    public void addBody(Node body) {
        this.exp = body;
    }

    public void addPar(Node par) { 
        this.parList.add(par);
    }

    public List<Node> getPars() { 
        return this.parList;
    }

    public String toPrint(final String s) {
        String parListStr = "";
        for (final Node par : parList)
            parListStr += par.toPrint(s + "  ");
        String decListStr = "";
        for (final Node dec : decList)
            decListStr += dec.toPrint(s + "  ");
        return s + "Fun:" + id + "\n" + symType.toPrint(s + "  ") + parListStr + decListStr + exp.toPrint(s + "  ");
    }

    /*
     * Per ogni variabile locale fa il type checking. Controlla se il tipo di ritorno
     * del body e' sottotipo di quello di ritorno della funzione. Altrimenti lancia
     * un'eccezione.
     */
    public Node typeCheck() throws TypeException {
        for (Node dec : decList)
            try {
                dec.typeCheck();
            } catch (TypeException e) {
                System.out.println("Type checking error in a declaration: " + e.text);
            }
        if (!FOOLlib.isSubtype(exp.typeCheck(), symType))
            throw new TypeException("Wrong return type for function " + this.id);
        return null;
    }

    public String codeGeneration() {
        String decListCode = "";                // code generation della lista di variabili locali
        String removeDecListCode = "";          // pop da effettuare per rimuovere dallo stack 
                                                // i valori allocati delle dichiarazioni
        String parCode = "";                    // pop dei parametri
        String funL = FOOLlib.freshFunLabel();  // label per la funzione, ottenuta in modo fresh

        /*
         * Nel caso di elementi funzionali e' necessario un "pop" aggiuntivo (occupano
         * offset doppio perche' oltre all'indirizzo della funzione contengono l'indirizzo
         * dell'AR in cui sono dichiarati).
         */
        for (int i = 0; i < decList.size(); i++) {
            if (decList.get(i) instanceof FunNode) {
                removeDecListCode += "pop\n";   // pop del codice dichiarazione se funzionale
            }
            decListCode += decList.get(i).codeGeneration();     // codice delle dichiarazioni
            removeDecListCode += "pop\n";                       // pop del codice dichiarazione
        }

        /*
         * Nel caso di elementi funzionali e' necessario un "pop" aggiuntivo (occupano
         * offset doppio perche' oltre all'indirizzo della funzione contengono l'indirizzo
         * dell'AR in cui sono dichiarati).
         */
        for (int i = 0; i < parList.size(); i++) {
            if (((DecNode) parList.get(i)).getSymType() instanceof ArrowTypeNode) {
                parCode += "pop\n";     // pop dei parametri se funzionale
            }
            parCode += "pop\n";         // pop dei parametri
        }

        FOOLlib.putCode(funL + ":\n" + "cfp\n" +        // setta il registro fp al valore di sp (indirizzo a cui si trova lo stack)
                "lra\n" +                               // mette sullo stack il contenuto di ra
                decListCode +                           // codice delle dichiarazioni
                exp.codeGeneration() + "stm\n" +        // fa la code generation del body e salva il risultato in un registro
                removeDecListCode +                     // svuota lo stack poppando tanti elementi quanti sono le dichiarazioni
                "sra\n" +                               // sullo stack dopo le pop c'e' il return address e lo salva in ra
                "pop\n" +                               // pop dell'AL (Access Link)
                parCode +                               // pop dei parametri di parlist
                "sfp\n" +                               // ripristina il registro fp al Control Link,
                                                        // in maniera che sia l'fp dell'ar del chiamante
                "ltm\n" + 								// pusha il risultato della funzione salvato in stm
                "lra\n" + 								// pusha il return address salvato in ra 
                "js\n"              					// js salta all'indirizzo che e' in cima allo stack e salva la prossima
                                                        // istruzione in ra
        );

        return "lfp\n" + "push " + funL + "\n";         // pusha l'indirizzo fp che contiene l'AR attuale e l'etichetta generata
    }

    @Override
    public Node getSymType() {
        return this.symType;
    }

}