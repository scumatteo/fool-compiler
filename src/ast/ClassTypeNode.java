package ast;

import java.util.ArrayList;
import java.util.List;

import lib.TypeException;

//Nodo di tipo classe. Contiene tutti i tipi di campi e metodi della classe.
public class ClassTypeNode implements Node {

	private List<Node> listAllFields; // lista di tutti i campi
	private List<Node> listAllMethods; // lista di tutti i metodi (ArrowTypeNode)

	public ClassTypeNode() {
		listAllFields = new ArrayList<>();
		listAllMethods = new ArrayList<>();
	}

	public void setField(Node field) {
		this.listAllFields.add(field);
	}

	public void setAllFields(List<Node> listFields) {
		this.listAllFields = listFields;
	}

	public List<Node> getAllFields() {
		return listAllFields;
	}

	public void setMethod(Node method) {
		this.listAllMethods.add(method);
	}

	public void setAllMethods(List<Node> listMethods) {
		this.listAllMethods = listMethods;
	}

	public List<Node> getAllMethods() {
		return listAllMethods;
	}

	@Override
	public String toPrint(String indent) {
		return null;
	}

	// typeCheck non utilizzato
	@Override
	public Node typeCheck() throws TypeException {
		return null;
	}

	// codeGeneration non utilizzato
	@Override
	public String codeGeneration() {
		return null;
	}

}
