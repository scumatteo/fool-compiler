package ast;

import lib.*;

// Classe che rappresenta un tipo ID di una qualsiasi variabile
public class IdNode implements Node {

    private String id;          // id della variabile
    private int nestingLevel;   // livello di nesting a cui viene utilizzata la variabile
    private STentry entry;      // le informazioni riguardo al nodo (tipo, offset) inserite in Symbol Table

    public IdNode(final String id, final STentry st, final int nl) {
        this.id = id;
        this.nestingLevel = nl;
        this.entry = st;
    }

    public String toPrint(final String s) {
        return s + "Id: " + id + "\n" + entry.toPrint(s + "  ");
    }

    public STentry getEntry() {
        return this.entry;
    }

    /*
     * Controlla se e' un nodo di tipo classe. Se lo e' lancia un'eccezione.
     * Controlla se e' un nodo di tipo method. Se lo e' lancia un'eccezione.
     * Altrimenti ritorna il tipo messo in Symbol Table.
     */
    public Node typeCheck() throws TypeException {
        if (entry.getType() instanceof ClassTypeNode)
            throw new TypeException("Type check found a problem: \n id can not be a class's name: " + this.id);
        if (entry.isMethod())
            throw new TypeException("Type check found a problem: \n id can not be a method: " + this.id);
        return entry.getType();
    }

    public String codeGeneration() {
        String getAR = "";

        for (int i = 0; i < nestingLevel - entry.getNestingLevel(); i++)
            getAR += "lw\n"; // risale la catena statica fino all'indirizzo dell'AR

        // Se non e' di tipo funzionale
        if (!(entry.getType() instanceof ArrowTypeNode)) {
            return "lfp\n" +                                    // pusha il control link
                    getAR +                                     // arriva al suo AR mediante risalita della catena statica
                    "push " + entry.getOffset() + "\n" +        // pusha l'offset della variabile
                    "add\n" +                                   // con la add ottiene il suo indirizzo
                    "lw\n";										// pusha il valore che c'e' a quell'indirizzo (della variabile)
        } else {
            /*
             * ArrowTypeNode: 
             * qualsiasi ID con tipo funzionale (vero ID di funzione oppure ID
             * di variabile o parametro di tipo funzione) occupa un offset doppio:
             * -> offset messo in Symbol Table, ovvero l'indirizzo (FP) dell'AR dove si trova la dichiarazione di funzione
             * -> offset messo in Symbol Table - 1, ovvero l'indirizzo di funzione (per invocazione del suo codice)
             */

            // Identica a CallNode, ma non fa il pop dei parametri e non salta a funzione
            return "lfp\n" +                                    // salva sullo stack l'fp del frame dove e' dichiarato l'id
                    getAR +                                     // pusha l'AL (punta al frame di dichiarazione dell'id)
                    "push " + entry.getOffset() + "\n" +        // pusha l'offset
                    "add\n" + 									// con la add ottiene l'indirizzo dell'id
                    "stm\n" + "ltm\n" +               			// duplica il top dello stack
                    "lw\n" +									// pusha sullo stack il contenuto a quell'indirizzo
                    "ltm\n" +                          			// ri-pusha l'indirizzo ottenuto precedentemente,
                                                                // per poi calcolare offset ID - 1
                    "push 1\n" +                                // push 1
                    "sub\n" +                                   // sottrae a offset ID - 1, per recuperare l'indirizzo dell'id funzionale
                    "lw\n";                                     // carica sullo stack
        }
    }
}