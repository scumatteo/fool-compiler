package ast;

import lib.*;

//Nodo per il tipo variabile.
public class VarNode implements Node, DecNode {

	private String id; // id della variabile
	private Node type; // tipo della variabile
	private Node exp; // espressione

	public VarNode(String i, Node t, Node v) {
		this.id = i;
		this.type = t;
		this.exp = v;
	}

	public String toPrint(String s) {
		return s + "Var:" + this.id + "\n" + this.type.toPrint(s + "  ") + this.exp.toPrint(s + "  ");
	}

	/*
	 * Controlla che il tipo di ritono messo in symbol table sia sottotipo del tipo
	 * di ritorno dell'espressione.
	 */
	public Node typeCheck() throws TypeException {
		if (!FOOLlib.isSubtype(this.exp.typeCheck(), this.type))
			throw new TypeException("Incompatible value for variable " + id);
		return null;
	}

	// ritorna il codice generato dall'espressione.
	public String codeGeneration() {
		return this.exp.codeGeneration();
	}

	public Node getSymType() {
		return this.type;
	}

}