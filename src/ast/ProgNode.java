package ast;

import lib.*;

//Nodo per il tipo programma in cui c'e' solo un'espressione, senza dichiarazioni
public class ProgNode implements Node {

	private Node exp; // espressione

	public ProgNode(Node e) {
		this.exp = e;
	}

	public String toPrint(String s) {
		return s + "Prog\n" + this.exp.toPrint(s + "  ");
	}

	// Effettua il typecheck sull'espressione
	public Node typeCheck() throws TypeException {
		return this.exp.typeCheck();
	}

	public String codeGeneration() {
		return this.exp.codeGeneration() + // ritorna il codice generato dell'espressione
				"halt\n"; // termina il programma
	}
}