package ast;

import java.util.List;

//Rappresenta un nodo funzionale a -> b
public class ArrowTypeNode implements Node {

	private List<Node> parList; //lista dei parametri
	private Node ret; //tipo di ritorno

	public ArrowTypeNode(List<Node> param, Node r) {
		parList = param;
		ret = r;
	}

	public Node getRet() {
		return ret;
	}

	public List<Node> getParList() {
		return parList;
	}

	public String toPrint(String s) {
		String parlstr = "";
		for (Node par : parList)
			parlstr += par.toPrint(s + "  ");
		return s + "ArrowTypeNode\n" + parlstr + ret.toPrint(s + "  ->");
	}

	// typeCheck non utilizzato
	public Node typeCheck() {
		return null;
	}

	// codeGeneration non utilizzato
	public String codeGeneration() {
		return "";
	}

}