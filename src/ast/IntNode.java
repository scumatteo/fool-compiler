package ast;

//Nodo per il valore del tipo "intero"
public class IntNode implements Node {

	private int value; // valore intero

	public IntNode( int n ) {
		value = n;
	}

	public String toPrint( String s ) {
		return s + "Int: " + Integer.toString( value ) + "\n";
	}

	// ritorna un IntTypeNode
	public Node typeCheck( ) {
		return new IntTypeNode( );
	}

	// pusha il valore sullo stack
	public String codeGeneration( ) {
		return "push " + value + "\n";
	}

}