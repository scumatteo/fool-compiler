package ast;

import lib.FOOLlib;
import lib.TypeException;

//Nodo per l'operatore "meno".
public class MinusNode implements Node {

	private Node left; 	//Sottoalbero di sinistra
	private Node right; //Sottoalbero di destra

	public MinusNode (Node l, Node r) {
		left = l;
		right = r;
	}

	@Override
	public String toPrint(String s) {
		return s + "Minus\n" + 
				left.toPrint(s+"  ") +
				right.toPrint(s+"  ") ; 
	}
	
	/*
	 * Se non sono entrambi degli interi tira un'eccezione, altrimenti ritorna un IntTypeNode
	 */
	@Override
	public Node typeCheck() throws TypeException {
		if ( ! ( FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode()) &&
				FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode()) ) ) 
			throw new TypeException("Non integers in subtraction");
		return new IntTypeNode( );
	}
	
	//Fa semplicemente la sub dell'SVM.
	@Override
	public String codeGeneration() {
		return left.codeGeneration()+			// valore del sottoalbero di sinistra
				right.codeGeneration()+			// valore del sottoalbero di destra
				"sub\n";
	}

}
