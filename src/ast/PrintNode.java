package ast;

import lib.*;

//Nodo di tipo print
public class PrintNode implements Node {

	private Node exp; //espressione da stampare.

	public PrintNode(Node e) {
		this.exp = e;
	}

	public String toPrint(String s) {
		return s + "Print\n" + this.exp.toPrint(s + "  ");
	}

	//Fa il typecheck sull'espressione
	public Node typeCheck() throws TypeException {
		return this.exp.typeCheck();
	}

	//Fa una semplice print della SVM.
	public String codeGeneration() {
		return this.exp.codeGeneration() + "print\n";
	}
}