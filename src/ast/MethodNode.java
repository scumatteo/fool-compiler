package ast;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lib.FOOLlib;
import lib.TypeException;

//Nodo per il tipo metodo
public class MethodNode implements DecNode, Node {

	private String id; //id del metodo
	private Node returnType; //tipo di ritorno
	private List<Node> parList = new ArrayList<Node>(); //lista dei parametri
	private List<Node> decList; //dichiarazione di variabili locali
	private Node bodyExpression; //body del metodo (espressione)
	private int offset; //offset a cui viene dichiarato
	private String label; //label del metodo controllata nell'estensione.

	public MethodNode(String i, Node t) {
		id = i;
		returnType = t;
		this.setOffset(-1);
		decList = new ArrayList<Node>( );
	}
	
	public String getID() {
		return this.id;
	}

	public void addDec(Node d) {
		decList.add(d);
	}

	public void addBody(Node b) {
		bodyExpression = b;
	}

	public void addPar(Node p) {
		parList.add(p);
	}
	
	public List<Node> getPars() { 
		return parList;
	}

	public String toPrint(String s) {
		String parlstr = parList.stream().map( p -> p.toPrint("  ") ).collect( Collectors.joining() );

		String declstr = decList.stream().map( d -> d.toPrint("  ") ).collect( Collectors.joining() );

		return s + "Method: " + id + "\n" + s + 
				"  return: " + returnType.toPrint("") + s + 
				"  params: " + parlstr + "\n" + s + 
				"  vars:" + declstr + "\n" + s + 
				"  body: \n" + bodyExpression.toPrint( s + "    " );
	}

	/*
	 * Per ogni variabile locale fa il typechecking. Controlla che il tipo di ritorno del body sia sottotipo
	 * del tipo di ritorno messo in symbol table. Se non lo e' tira un'eccezione.
	 */
	public Node typeCheck() throws TypeException {
		decList.forEach( declaration -> {
			try {
				declaration.typeCheck( );		// type check per ogni variabile locale
			} catch ( TypeException e ) {		// se cattura un errore, lo stampa e prova il resto delle dichiarazioni 
												// in modo da trovare il maggior numero di errori in un solo type-check
				System.out.println("Type checking error in a declaration in method: " + e.text);
			}
		} );
			
		// se il tipo dell'espressione non coincide col tipo restituito dal metodo tira un eccezione
		if ( ! FOOLlib.isSubtype(bodyExpression.typeCheck(), returnType))
			throw new TypeException("Wrong return type in method " + id);
		return null;
	}

	public String codeGeneration() {
		label = FOOLlib.freshMethodLabel(); //genera etichetta per i metodi

		String declCode = decList.stream( ) 	//codice delle variabili locali
				.map( Node::codeGeneration )	//codice delle dichiarazioni
				.collect( Collectors.joining( ) );
		
		//fa tante pop quante sono le variabili dichiarate internamente al metodo. Possono essere solo var e non
		//tipi funzionali
		String removeDecListCode = decList.stream( )	//pop per le variabili locali
				.map( e -> "pop\n" )				//pop del codice dichiarazione se NON funzionale
				.collect( Collectors.joining( ) );

		String parCode = parList.stream( ) 		//pop per i parametri
				.map( e -> ( DecNode ) e ).map( DecNode::getSymType )
				.map( e -> e instanceof ArrowTypeNode
						? "pop\n" + "pop\n" 	//pop dei parametri se funzionale
						: "pop\n" )				//pop dei parametri se NON funzionale
				.collect( Collectors.joining( ) );

		FOOLlib.putCode(
				label + ":\n" + 
				"cfp\n" + // setta il registro fp al valore di sp (indirizzo a cui si trova lo stack)
				"lra\n" + // mette sullo stack il contenuto di ra
				declCode + // codice delle dichiarazioni
				bodyExpression.codeGeneration() + "stm\n" + // salva il risultato nel registro stm
				removeDecListCode + // svuota lo stack poppando tanti elementi quanti sono le dichiarazioni
				"sra\n" + // sullo stack dopo le pop c'e' il return address e lo salva in ra
				"pop\n" + // pop dell'AL (access link)
				parCode + // pop dei parametri di parlist
				"sfp\n" +  // ripristina il registro fp al Control Link,
                			// in maniera che sia l'fp dell'ar del chiamante
				"ltm\n" +  // pusha il risultato della funzione salvato in stm
                "lra\n" + // pusha il return address salvato in ra 
				"js\n" // js salta all'indirizzo che e' in cima allo stack e salva la prossima
						// istruzione in ra.
		);
			
		return "";
	}

	@Override
	public Node getSymType() {
		return returnType;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getLabel() {
		return label;
	}

}
