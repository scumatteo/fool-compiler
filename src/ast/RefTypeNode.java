package ast;

import lib.TypeException;

//Nodo per il tipo riferimento alla classe.
public class RefTypeNode implements Node {

	private String id; //id del riferimento
	
	
	public RefTypeNode( String id ) {
		this.id = id;
	}
	
	public String getID( ) {
		return this.id;
	}
	
	@Override
	public String toPrint(String indent) {
		return indent + "Reference of class: " + this.id + "\n" ;
	}

	@Override
	public Node typeCheck() throws TypeException {
		return null;
	}

	@Override
	public String codeGeneration() {
		return null;
	}

}
