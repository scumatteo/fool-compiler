package ast;

// Nodo per il tipo null
public class EmptyNode implements Node {

    public EmptyNode() {
    }

    public String toPrint(final String s) {
        return s + "Null\n";
    }

    // Restituisce un tipo empty
    public Node typeCheck() {
        return new EmptyTypeNode();
    }

    // Mette -1 sullo stack perche' -1 e' diverso da qualsiasi indirizzo dello stack
    public String codeGeneration() {
        return "push -1\n";
    }
}
