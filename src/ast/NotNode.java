package ast;
import lib.TypeException;

//Classe per il tipo not !.
public class NotNode implements Node {

	private Node expression; //espressione da negare

	public NotNode (Node e) {
		expression = e;
	}

	public String toPrint(String s) {
		return s + "Not\n" + expression.toPrint(s+"  ") ;
	}

	/*
	 * Consente il not solo ai booleani. Se l'exp non e' booleano tira un'eccezione.
	 * Altrimenti ritorna un BoolTypeNode
	 */
	public Node typeCheck() throws TypeException {
		Node expressionType = expression.typeCheck(); 
		if ( ! ( expressionType instanceof BoolTypeNode ) ) 
			throw new TypeException("Incompatible type in not");
		return new BoolTypeNode();
	}

	/* 
	 * Per negare un'espressione booleana essendo tale valore solo o 0 (false) o 1 (true) basta semplicemente
	 * sottrarre a 1 il valore di tale espressione. 
	 * Se e' true => 1 - 1 = 0 => false
	 * Se e' false => 1 - 0 = 1 => true
	 */
	public String codeGeneration() {
		return "push 1\n" + 			//pusha 1
				expression.codeGeneration() + 	//pusha l'espressione booleana 
				"sub\n"; 				//fa la sottrazione
	}

}
