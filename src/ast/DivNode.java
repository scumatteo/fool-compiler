package ast;

import lib.FOOLlib;
import lib.TypeException;

//Implementa il nodo per l'operazione divisione
public class DivNode implements Node {

	private Node left; //Sottoalbero di sinistra
	private Node right;//Sottoalbero di destra

	public DivNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	public String toPrint(String s) {
		return s + "Div\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}

	/*
	 * Permette di farlo solo a interi. Se almeno uno dei due non e' intero tira un'eccezione.
	 * Torna un nodo di tipo Int.
	 */
	
	public Node typeCheck() throws TypeException {
		if (!(FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode())
				&& FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode())))
			throw new TypeException("Non integers in division");
		return new IntTypeNode();
	}
	
	/*
	 * Fa semplicemente l'operazione div della SVM.
	 * Il left e' il dividendo e il right il divisore.
	 */
	
	public String codeGeneration() {
		return left.codeGeneration() + right.codeGeneration() + "div\n";
	}
	

}
