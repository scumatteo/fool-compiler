package ast;


//Classe che rappresenta una entry della symbol table.
public class STentry {
   
  private int nestingLevel; //livello di nesting a cui e' stata aggiunta la entry
  private Node type; //tipo rappresentato dalla entry
  private int offset; //offset dell'activation record o, per campi e metodi, della Virtual table 
  						//a cui e' stata aggiunta la entry
  private boolean isMethod; //booleano che controlla se e' un metodo

  public STentry (int nestingLevel, Node type, int offset, boolean isMethod) {
	  this.nestingLevel=nestingLevel;
	  this.type=type;
	  this.offset=offset;
	  this.isMethod = isMethod;
  }
  
  public Node getType() {
	  return type;
  }
  
  public int getOffset() {
	  return offset;
  }
  
  public int getNestingLevel() {
	  return nestingLevel;
  }
  
  public String toPrint(String s) {
	   return s+"STentry: nestlev " + nestingLevel +"\n"+
			  s+"STentry: type\n " +
			      type.toPrint(s+"  ") +
			  s+"STentry: offset " + offset +"\n"+
			  s+"STentry: is " + (isMethod ? "" : "not ") + "method\n";
  }
  
  public boolean isMethod() {
	  return this.isMethod;
  }
  
}  