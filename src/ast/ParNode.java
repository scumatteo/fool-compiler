package ast;

//Nodo per i tipi parametri di funzione.
public class ParNode implements Node, DecNode {

	private String id; //id del parametro
	private Node type; //tipo del parametro

	public ParNode(String i, Node t) {
		this.id = i;
		this.type = t;
	}

	public String toPrint(String s) {
		return s + "Par:" + this.id + "\n" + this.type.toPrint(s + "  ");
	}

	// non utilizzato
	public Node typeCheck() {
		return null;
	}

	// non utilizzato
	public String codeGeneration() {
		return "";
	}

	@Override
	public Node getSymType() {
		return this.type;
	}
	
	public String getId() {
		return this.id;
	}

}