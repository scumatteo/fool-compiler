package ast;

// Tipo null. Utilizzato da EmptyNode come tipo di ritorno.
public class EmptyTypeNode implements Node {

    public EmptyTypeNode() {
    }

    public String toPrint(final String s) {
        return s + "EmptyType\n";
    }

    // Non utilizzato
    public Node typeCheck() {
        return null;
    }

    // Non utilizzato
    public String codeGeneration() {
        return "";
    }
}
