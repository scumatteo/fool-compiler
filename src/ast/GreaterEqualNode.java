package ast;

import lib.*;

// Nodo per rappresentare il >=
public class GreaterEqualNode implements Node {

    private Node left;  // sottoalbero di sinistra
    private Node right; // sottoalbero di destra

    public GreaterEqualNode(final Node l, final Node r) {
        this.left = l;
        this.right = r;
    }

    public String toPrint(final String s) {
        return s + "GreaterEqual\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
    }

    /*
     * Controlla che i due nodi siano uno sottotipo dell'altro. Se non lo sono lancia
     * un'eccezione.
     */
    public Node typeCheck() throws TypeException {
        Node l = left.typeCheck();
        Node r = right.typeCheck();
        if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l)))
            throw new TypeException("Incompatible types in  greaterequal");
        return new BoolTypeNode();
    }
  
    /*
     * Non avendo come istruzione della SVM il >=, si puo' semplicemente invertire il
     * valore degli elementi sullo stack e utilizzare la procedura di <=.
     */
    public String codeGeneration() {
        String l1= FOOLlib.freshLabel();
        String l2= FOOLlib.freshLabel();
        return  right.codeGeneration() +
                left.codeGeneration() +
                "bleq " + l1 + "\n" +   // se e' <= salta a l1
                "push 0\n" +            // in caso negativo pusha 0 (false)
                "b " + l2 + "\n" +      // e salta direttamente a l2
                l1 + ": \n" +           // se e' <= arriva qui
                "push 1\n" +            // in caso positivo pusha 1 (true)
                l2+": \n";
    }
}  