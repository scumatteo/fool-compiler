package ast;

// Nodo che implementa il tipo campo di una classe
public class FieldNode implements DecNode, Node {

    private String id;    // id del campo
    private Node symType; // tipo di ritorno (inserito in Symbol Table)
    private int offset;   // offset a cui e' dichiarato nella dichiarazione di classe

    public FieldNode(final String id, final Node n) {
        this.id = id;
        this.symType = n;
        this.offset = 0;
    }

    public String toPrint(final String s) {
        return s + "Field: " + id + "\n" + symType.toPrint(s + "  ");
    }

    // Non utilizzato
    public Node typeCheck() {
        return null;
    }

    // Non utilizzato
    public String codeGeneration() {
        return "";
    }

    @Override
    public Node getSymType() {
        return this.symType;
    }

    public int getOffset() {
        return this.offset;
    }

    public void setOffset(int off) {
        this.offset = off;
    }

    public String getId() {
        return this.id;
    }

}
