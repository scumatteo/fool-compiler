package lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ast.*;

public class FOOLlib {

	public static final int MEMSIZE = 10000;
	public static int typeErrors = 0;
	public static Map<String, String> superType = new HashMap<>();
	public static List<List<String>> dispatchTable = new ArrayList<>();

	private static int labelCount = 0; // numero di label per i salti
	private static int funlabelCount = 0; // numero di label delle funzioni
	private static int methodlabelCount = 0; // numero di label dei metodi
	private static String funCode = ""; // codice delle funzioni

	public static String freshLabel() {
		return "label" + (labelCount++);
	}

	public static String freshFunLabel() {
		return "function" + (funlabelCount++);
	}

	public static String freshMethodLabel() {
		return "method" + (methodlabelCount++);
	}

	public static void putCode(String c) {
		funCode += "\n" + c; // aggiunge una linea vuota di separazione prima di funzione
	}

	public static String getCode() {
		return funCode;
	}

	public static boolean isSubtype(Node a, Node b) {

		// Se a e' EmptyTypeNode b deve essere RefTypeNode, altrimenti torna false
		if ((a instanceof EmptyTypeNode)) {
			return (b instanceof RefTypeNode);
		}
		// Se arrivo qui vuol dire che a non e' EmptyTypeNode e se b lo e' torna false
		// perche' non puo'
		// essere sottotipo
		if (b instanceof EmptyTypeNode) {
			return false;
		}

		// Se sono entrambi RefTypeNode prende l'id di entrambi
		if ((a instanceof RefTypeNode) && (b instanceof RefTypeNode)) {
			String idA = ((RefTypeNode) a).getID();
			String idB = ((RefTypeNode) b).getID();

			// Risale la catena di superclassi di a e se a un certo punto il tipo e' uguale a
			// quello
			// di b allora a e' sottotipo di b. Altrimenti se si arriva alla classe padre di
			// entrambi senza
			// trovare un tipo comune si ha che a non e' sottotipo di b.
			while (!(idB.equals(idA)) && idA != null) {
				idA = superType.get(idA);
			}
			return (idA!= null);
		}

		

		/*
		 * Controlla se entrambi sono ArrowTypeNode. Se lo sono, controlla che il numero
		 * di parametri sia uguale altrimenti torna false. Per progettazione si
		 * considerano diversi due ArrowTypeNode con diverso numero dei parametri. Se
		 * hanno stesso numero di parametri allora controlla la covarianza sul tipo di
		 * ritorno e la controvarianza sul tipo dei parametri.
		 */

		// Nel caso di tipi funzionali
		Node retA;
		Node retB;
		if ((a instanceof ArrowTypeNode) && (b instanceof ArrowTypeNode)) {
			// Sono entrambi elementi di tipo funzionale.
			// Si controlla il tipo di ritorno
			retA = ((ArrowTypeNode) a).getRet(); // valore di ritorno della prima funzione
			retB = ((ArrowTypeNode) b).getRet(); // valore di ritorno della seconda funzione
			// Deve essere valida la proprieta' di covarianza (cioe' a deve essere sottotipo
			// di b)
			if (!(isSubtype(retA, retB)))
				return false;

			// Si controllano i tipi dei parametri richiesti in input.
			// Si controlliano a coppie, se anche solo una coppia ha elementi di classi
			// diverse si ritorna "false".
			List<Node> parListA = ((ArrowTypeNode) a).getParList();
			List<Node> parListB = ((ArrowTypeNode) b).getParList();
			// Per prima cosa il numero di parametri richiesti in ingresso deve essere lo
			// stesso per entrambi.
			if (parListA.size() != parListB.size()) {
				return false;
			}
			for (int i = 0; i < parListA.size(); i++) {
				// proprieta' di controvarianza (cioe' i-esimo par di b deve essere sottotipo
				// dell'i-esimo par di a)
				if (!(isSubtype(parListB.get(i), parListA.get(i))))
					return false;
			}

			return true;
		}

		// Se non e' RefTypeNode o EmptyTypeNode o ArrowTypeNode ritorna se a e' sottotipo di b
		Class<? extends Node> left = a.getClass();
		Class<? extends Node> right = b.getClass();
		
		return left.equals(right) || ((a instanceof BoolTypeNode) && (b instanceof IntTypeNode));

	}

	/*
	 * Metodo introdotto per l'ottimizzazione nel nodo if then else. Se nell'if then
	 * else, un ramo ritorna B e un ramo C, va bene a patto che abbiano un antenato
	 * comune A. 
	 * 	 A
	 *  / \ 
	 *  B C
	 */
	public static Node lowestCommonAncestor(Node a, Node b) {

		// Altrimenti se sono RefTypeNode si cerca il lowest common ancestor, risalendo
		// la catena:
		// se b e sottotipo di a o di un padre di a ritorna a
		if (a instanceof RefTypeNode && b instanceof RefTypeNode) {
			String idA = ((RefTypeNode) a).getID();
			RefTypeNode superA = new RefTypeNode(superType.get(idA));
			while (superA != null) {
				if (isSubtype(b, superA))
					return superA;

				idA = superType.get(idA);
				superA = new RefTypeNode(superType.get(idA));
			}
		}

		// Se uno dei due e' empty ritorno l'altro tipo semplicemente, perche' null puo'
		// essere qualsiasi cosa.
		if ((a instanceof EmptyTypeNode))
			return b;

		if ((b instanceof EmptyTypeNode))
			return a;

		// se sono ArrowTypeNode
		if (a instanceof ArrowTypeNode && b instanceof ArrowTypeNode) {
			ArrowTypeNode arrowA = (ArrowTypeNode) a;
			ArrowTypeNode arrowB = (ArrowTypeNode) b;

			// Controlla la size dei parametri e se sono diversi torna null
			if (arrowA.getParList().size() != arrowB.getParList().size()) {
				return null;
			}
			// altrimenti chiama ricorsivamente lowestCommonAncestor sul tipo di ritorno
			Node lowestCommonAncestor = lowestCommonAncestor(arrowA.getRet(), arrowB.getRet());
			// se e' null torna il lowestCommonAncestor cioe' null
			if (lowestCommonAncestor == null) {
				return lowestCommonAncestor;
			}
			List<Node> parList = new ArrayList<Node>();
			for (int i = 0; i < arrowA.getParList().size(); i++) {
				if (!(isSubtype(arrowA.getParList().get(i), arrowB.getParList().get(i)))
						&& !(isSubtype(arrowB.getParList().get(i), arrowA.getParList().get(i)))) {
					return null;
				} else if (isSubtype(arrowA.getParList().get(i), arrowB.getParList().get(i))) {
					parList.add(arrowA.getParList().get(i));
				} else {
					parList.add(arrowB.getParList().get(i));
				}

			}
			return new ArrowTypeNode(parList, lowestCommonAncestor);
		}

		// Controlla tra int e bool e ritorna il piu' grande
		if ((a instanceof BoolTypeNode) && (b instanceof BoolTypeNode))
			return new BoolTypeNode();
		if ((a instanceof BoolTypeNode) && (b instanceof IntTypeNode)
				|| (a instanceof IntTypeNode) && (b instanceof BoolTypeNode)
				|| (a instanceof IntTypeNode) && (b instanceof IntTypeNode))
			return new IntTypeNode();

		// se tutti i controlli precedenti non passano torna null
		return null;
	}

}
